Prepare "MP4 movie file only",
even if your any terminal does not support FLASH player,
if it supports HTML - 5 (video), it will give priority,
and in some cases will automatically switch to FLASH player.

The publishing server can be "download type" or "streaming type".

Please check on the sample page, iPhone or Android terminal first.  https://gitlab.com/html5_video/201701/issues

All you have to do is prepare an MP4 video file and upload it to the public server.

_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/

「ＭＰ４動画ファイルのみ」を用意して、FLASHプレイヤーをサポートしていない端末であってもHTML-5(video)をサポートするなら、それを優先し、場合によってFLASHプレイヤーに自動的に切り替わる。
  公開サーバーが「ダウンロードタイプ」でも「ストリーミングタイプ」でも構いません。

先ずはサンプルページで、iPhoneやAndroid端末で確認願います。  https://gitlab.com/html5_video/201701/issues

あなたはＭＰ４動画ファイルを用意し、公開サーバーにアップロードするだけです。